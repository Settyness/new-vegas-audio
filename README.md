
# ♠️ New Vegas Audio

This is a work in progress guide for improving various audio elements in Fallout New Vegas.

#### 📻️ Radio New Vegas Fix

<div align="center">

<img src="./img/radio-new-vegas-fix.jpg" alt="Radio New Vegas Fix" />

[Radio New Vegas Fix at Nexus Mods](https://www.nexusmods.com/newvegas/mods/37459)

</div>

This fixes a scripting bug with the radio wherein "_Big Iron_" plays twice as much as it should (in place of "_Stars of the Midnight Range_") and sometimes even twice in a row. It is _not_ covered by Yukichigai Unofficial Patch. This also adds four songs, performed by Josh Sawyer, the game's director, that were originally intended to play on the radio. Included is also a continuity fix for the radio at Trudy's bar that The Courier repairs: she says she enjoys Mr. New Vegas' voice, which implies she listens to Radio New Vegas, but Mojave Music Radio plays instead. If you don't want the Josh Sawyer songs, you can install the "Bugfix Version", but I recommend the main file. You can load this directly after Yukichigai Unofficial Patch.

#### 🔊 Normalize Radio Tracks

For whatever reason, Bethesdsa saw fit to normalize the MP3 files for the radio at around -9 dB. It is possible the developers thought the radio would overwhelm dialogue scenes, but in practice this never occurs, as the radio lowers in volume during POV dialogue. It's also the principle of the matter: if I want to headbang to "_Blue Moon_" while plugging idiots, it's my goddamn right to do so at max volume! So here's how to set that up:</p>

<div align="center">

<img src="./img/mp3gain.jpg" alt="MP3Gain" />

[MP3Gain at SourceForge](https://mp3gain.sourceforge.net/download.php)

</div>

Download `mp3gain-win-full-1_2_5.zip` and extract it (you can download the installer, but there's no sense in creating registry keys for this application if you don't intend on using it for anything else). Execute `MP3GainGUI.exe`, navigate to your Fallout New Vegas game directory, and drag the contents of the following folders into the MP3Gain window:

- `Data\Sound\songs\radionv`
- `Data\Sound\songs\radionvdlc01`
- `Data\Sound\songs\radionvdlc03`

Click the "Track Gain" button up top and wait for the process to finish. You may notice `MUS_Its_A_Sin_To_Tell_A_Lie.mp3` turns red. This is because there's some clipping in this version of the song, right before the singer wails. The distortion present occurs at any volume, so there is little point in stressing about it. After the process finishes, the songs will have been normalized, you are set. Now the radio volume slider in-game is accurate to your ear holes.

#### ♻️ Contributing

This guide is open to contributions (specifications, corrections, addendums, etc...) via merge requests and I will also do my best to support anyone who files issues in this repository.
